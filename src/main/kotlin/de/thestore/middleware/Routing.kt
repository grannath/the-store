package de.thestore.middleware

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController

@RestController
class Routing(private val restaurants: List<Restaurant>, private val distributor: Distributor) {
    @GetMapping("/restaurants")
    suspend fun getRestaurants() =
        restaurants.map { it.id }

    // Suspend function is possible here starting Spring Framework 5.2
    // Means that WebFlux does not block here, but we also don't have to mess with Mono and other reactive types
    @PostMapping("/orders")
    @ResponseStatus(HttpStatus.CREATED)
    suspend fun postOrder(@RequestBody order: Order) =
        distributor.distribute(order)

    @ExceptionHandler(UnknownItemException::class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    suspend fun handleUnknownItem(e: UnknownItemException) =
        e.message

    @ExceptionHandler(UnknownRestaurantException::class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    suspend fun handleUnknownRestaurant(e: UnknownRestaurantException) =
        e.message

    @ExceptionHandler(OutOfItemException::class)
    @ResponseStatus(HttpStatus.SERVICE_UNAVAILABLE)
    suspend fun handleOutOfItem(e: OutOfItemException) =
        e.message
}

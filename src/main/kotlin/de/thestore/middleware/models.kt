package de.thestore.middleware

import java.time.OffsetDateTime

interface Restaurant {
    val id: RestaurantId

    // Declared as suspend, as this will in reality involve IO
    // With coroutines, this can be done nonblocking while keeping the code clean
    suspend fun submitOrder(order: Order): Confirmation
}

// Inline classes give type safety, but are compiled into bare values, so have no overhead
inline class RestaurantId(val id: String)
inline class ItemId(val id: String)

// No validations here yet
data class Order(
    val items: List<Item>,
    val restaurantId: RestaurantId,
    val deliveryAddress: Address
)

data class Item(
    val id: ItemId,
    val customerComment: String
)

data class Address(
    val name: String,
    val company: String? = null,
    val addressLineOne: String,
    val addressLineTwo: String? = null,
    val postalCode: String,
    val city: String
)

data class Confirmation(
    val deliveryTime: OffsetDateTime
)

class WrongRestaurantException(message: String) : RuntimeException(message)

class UnknownItemException(message: String) : RuntimeException(message)

class OutOfItemException(message: String) : RuntimeException(message)

package de.thestore.middleware

import kotlinx.coroutines.delay
import org.springframework.stereotype.Component
import java.time.OffsetDateTime
import kotlin.random.Random

private val validItems = listOf(
    ItemId("salmon-bowl"),
    ItemId("vegan-bowl"),
    ItemId("chicken-wrap")
)

private const val BASE_DELIVERY_DURATION = 15L
private const val MAX_ADDITIONAL_DELIVERY_DURATION = 45L

@Component
class FineRestaurant : Restaurant {
    override val id: RestaurantId = RestaurantId("fine-restaurant")

    override suspend fun submitOrder(order: Order): Confirmation {
        isMy(order)

        order.hasOnlyValidItems()

        return Confirmation(
            deliveryTime = OffsetDateTime.now().plusMinutes(
                BASE_DELIVERY_DURATION + Random.nextLong(
                    MAX_ADDITIONAL_DELIVERY_DURATION
                )
            )
        )
    }
}

private const val MAX_DELAY = 5000L

@Component
class SlowRestaurant : Restaurant {
    override val id: RestaurantId = RestaurantId("slow-restaurant")

    override suspend fun submitOrder(order: Order): Confirmation {
        // Similar effect to Thread.sleep, but only suspends the coroutine
        // No thread is actually blocked here
        delay(Random.nextLong(MAX_DELAY))

        isMy(order)

        order.hasOnlyValidItems()

        return Confirmation(
            deliveryTime = OffsetDateTime.now().plusMinutes(
                BASE_DELIVERY_DURATION + Random.nextLong(
                    MAX_ADDITIONAL_DELIVERY_DURATION
                )
            )
        )
    }
}

@Component
class BadRestaurant : Restaurant {
    override val id: RestaurantId = RestaurantId("bad-restaurant")

    override suspend fun submitOrder(order: Order): Confirmation {
        isMy(order)

        order.hasOnlyValidItems()

        if (order.items.any { it.id == ItemId("chicken-wrap") }) {
            throw OutOfItemException("Restaurant ${this.id} is out of chicken-wrap.")
        }

        return Confirmation(
            deliveryTime = OffsetDateTime.now().plusMinutes(
                BASE_DELIVERY_DURATION + Random.nextLong(
                    MAX_ADDITIONAL_DELIVERY_DURATION
                )
            )
        )
    }
}

private fun Restaurant.isMy(order: Order) {
    if (order.restaurantId != this.id) {
        throw WrongRestaurantException("This is $id, not ${order.restaurantId}.")
    }
}

private fun Order.hasOnlyValidItems() {
    if (!validItems.containsAll(items.map { it.id })) {
        throw UnknownItemException(
            "These items are unknown:\n${items.filter { !validItems.contains(it.id) }.joinToString()}"
        )
    }
}

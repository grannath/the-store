package de.thestore.middleware

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class TheStoreApplication

@Suppress("SpreadOperator")
fun main(args: Array<String>) {
    runApplication<TheStoreApplication>(*args)
}

package de.thestore.middleware

import org.springframework.stereotype.Component

@Component
class Distributor(private val restaurants: List<Restaurant>) {
    suspend fun distribute(order: Order): Confirmation =
        // Concise way to handle missing items
        restaurants.singleOrNull { it.id == order.restaurantId }?.submitOrder(order)
            ?: throw UnknownRestaurantException("Unknown restaurant ${order.restaurantId}")
}

class UnknownRestaurantException(message: String) : RuntimeException(message)

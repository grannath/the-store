package de.thestore.middleware

import io.kotlintest.shouldThrow
import io.kotlintest.specs.StringSpec
import io.kotlintest.spring.SpringListener
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.stereotype.Component
import java.time.OffsetDateTime

@Component
class FakeRestaurant : Restaurant {
    override val id: RestaurantId = RestaurantId("fake")

    override suspend fun submitOrder(order: Order): Confirmation {
        if (order.restaurantId != this.id) {
            throw WrongRestaurantException("This is restaurant $id, but the order is for ${order.restaurantId}.")
        }

        return Confirmation(deliveryTime = OffsetDateTime.now().plusMinutes(30))
    }
}

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
class GenericRestaurantTests : StringSpec() {

    override fun listeners() = listOf(SpringListener)

    @Autowired
    lateinit var restaurants: List<Restaurant>

    init {
        "when submitting order with wrong id, exception is thrown" {
            restaurants.forEach { restaurant ->
                shouldThrow<WrongRestaurantException> {
                    restaurant.submitOrder(
                        Order(
                            items = listOf(),
                            restaurantId = RestaurantId("foo"),
                            deliveryAddress = Address(
                                name = "Max",
                                addressLineOne = "Straße 1",
                                postalCode = "12345",
                                city = "Berlin"
                            )
                        )
                    )
                }
            }
        }
    }
}

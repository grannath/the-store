package de.thestore.middleware

import io.kotlintest.specs.StringSpec
import io.kotlintest.spring.SpringListener
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.web.reactive.server.WebTestClient

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebTestClient
class EndpointsTests : StringSpec() {
    override fun listeners() = listOf(SpringListener)

    @Autowired
    lateinit var webClient: WebTestClient

    init {
        "/restaurants returns known restaurants" {
            webClient.get().uri("/restaurants").exchange().expectStatus().isOk.expectBody().jsonPath(".[*]").isArray
        }

        "/orders accepts valid orders" {
            webClient.post()
                .uri("/orders")
                .bodyValue(
                    Order(
                        items = listOf(), restaurantId = RestaurantId("fine-restaurant"), deliveryAddress = Address(
                            name = "Max",
                            addressLineOne = "Straße 1",
                            postalCode = "23456",
                            city = "Hamburg"
                        )
                    )
                )
                .exchange()
                .expectStatus()
                .isCreated
        }

        "/orders complains on invalid orders" {
            webClient.post()
                .uri("/orders")
                .bodyValue(
                    Order(
                        items = listOf(), restaurantId = RestaurantId("unknown-restaurant"), deliveryAddress = Address(
                            name = "Max",
                            addressLineOne = "Straße 1",
                            postalCode = "23456",
                            city = "Hamburg"
                        )
                    )
                )
                .exchange()
                .expectStatus()
                .isBadRequest
        }
    }
}

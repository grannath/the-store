package de.thestore.middleware

import io.kotlintest.data.suspend.forall
import io.kotlintest.shouldNotThrowAny
import io.kotlintest.shouldThrow
import io.kotlintest.specs.StringSpec
import io.kotlintest.spring.SpringListener
import io.kotlintest.tables.row
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
class DistributionTests : StringSpec() {
    // Unfortunately necessary with Kotlintest
    override fun listeners() = listOf(SpringListener)

    @Autowired
    lateinit var distributor: Distributor

    init {
        // One of many ways to define readable tests in Kotlintest
        "distributes orders to known restaurants" {
            // Define tables of data and test all rows, very convenient
            forall(row("fine-restaurant"), row("slow-restaurant"), row("bad-restaurant")) { id: String ->
                shouldNotThrowAny {
                    distributor.distribute(
                        Order(
                            items = listOf(),
                            restaurantId = RestaurantId(id),
                            deliveryAddress = Address(
                                name = "Max",
                                addressLineOne = "Straße 1",
                                postalCode = "23456",
                                city = "Hamburg"
                            )
                        )
                    )
                }
            }
        }

        "throws exception on unknown restaurants" {
            forall(row("not-my-restaurant"), row("slow-restauran")) { id: String ->
                shouldThrow<UnknownRestaurantException> {
                    distributor.distribute(
                        Order(
                            items = listOf(),
                            restaurantId = RestaurantId(id),
                            deliveryAddress = Address(
                                name = "Max",
                                addressLineOne = "Straße 1",
                                postalCode = "23456",
                                city = "Hamburg"
                            )
                        )
                    )
                }
            }
        }
    }
}
